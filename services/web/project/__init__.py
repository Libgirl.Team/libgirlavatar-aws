from flask import Flask
import time
from project.after_response import AfterResponse

app = Flask(__name__)
AfterResponse(app)

@app.after_response
def long_running_task():
    print("Starting long running task")
    time.sleep(5)
    print("Long running task finished")

@app.route("/run")
def handle_request():
    return "", 200
